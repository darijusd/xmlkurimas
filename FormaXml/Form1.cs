﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FormaXml
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            if (!string.IsNullOrEmpty(txtbVardas.Text))
            {
                var asmuo = new Asmuo
                {
                    Id = -1,
                    Vardas = txtbVardas.Text,
                    Pavarde = txtbPavarde.Text,
                    Amzius = txtbAmzius.Text,
                    Adresas = new Asmuo._Adresas()
                    {
                        Miestas = txtbMiestas.Text,
                        Gatve = txtbGatve.Text,
                        Namas = txtbNamas.Text
                    }

                };
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("root");
                doc.AppendChild(root);
                XmlElement vardas = doc.CreateElement("vardas");
                vardas.InnerText = asmuo.Vardas;
                root.AppendChild(vardas);
                XmlElement pavarde = doc.CreateElement("pavarde");
                pavarde.InnerText = asmuo.Pavarde;
                root.AppendChild(pavarde);
                XmlElement amzius = doc.CreateElement("amzius");
                amzius.InnerText = asmuo.Amzius;
                root.AppendChild(amzius);

                XmlElement adresas = doc.CreateElement("adresas");
                root.AppendChild(adresas);
                XmlElement miestas = doc.CreateElement("miestas");
                miestas.InnerText = asmuo.Adresas.Miestas;
                adresas.AppendChild(miestas);
                XmlElement gatve = doc.CreateElement("gatve");
                gatve.InnerText = asmuo.Adresas.Gatve;
                adresas.AppendChild(gatve);
                XmlElement namas = doc.CreateElement("namas");
                namas.InnerText = asmuo.Adresas.Namas;
                adresas.AppendChild(namas);
            }
            else
            {
                MessageBox.Show("Neivedete vardo");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtbPavarde_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
