﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormaXml
{
    class Asmuo
    {
        public int Id { get; set; }

        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public string Amzius { get; set; }

        public FormaXml.Asmuo._Adresas Adresas { get; set; }

        public class _Adresas
        {
            public string Miestas { get; set; }

            public string Gatve { get; set; }

            public string Namas { get; set; }
        }
    }
}
