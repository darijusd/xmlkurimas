﻿namespace FormaXml
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtbVardas = new System.Windows.Forms.TextBox();
            this.txtbPavarde = new System.Windows.Forms.TextBox();
            this.txtbMiestas = new System.Windows.Forms.TextBox();
            this.txtbGatve = new System.Windows.Forms.TextBox();
            this.txtbAmzius = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtbNamas = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vardas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pavarde";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Miestas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Namas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Amzius";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(253, 299);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 30);
            this.button1.TabIndex = 5;
            this.button1.Text = "Send";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtbVardas
            // 
            this.txtbVardas.Location = new System.Drawing.Point(93, 25);
            this.txtbVardas.Name = "txtbVardas";
            this.txtbVardas.Size = new System.Drawing.Size(100, 20);
            this.txtbVardas.TabIndex = 6;
            // 
            // txtbPavarde
            // 
            this.txtbPavarde.Location = new System.Drawing.Point(93, 51);
            this.txtbPavarde.Name = "txtbPavarde";
            this.txtbPavarde.Size = new System.Drawing.Size(100, 20);
            this.txtbPavarde.TabIndex = 7;
            this.txtbPavarde.TextChanged += new System.EventHandler(this.txtbPavarde_TextChanged);
            // 
            // txtbMiestas
            // 
            this.txtbMiestas.Location = new System.Drawing.Point(93, 132);
            this.txtbMiestas.Name = "txtbMiestas";
            this.txtbMiestas.Size = new System.Drawing.Size(100, 20);
            this.txtbMiestas.TabIndex = 8;
            // 
            // txtbGatve
            // 
            this.txtbGatve.Location = new System.Drawing.Point(93, 158);
            this.txtbGatve.Name = "txtbGatve";
            this.txtbGatve.Size = new System.Drawing.Size(100, 20);
            this.txtbGatve.TabIndex = 9;
            // 
            // txtbAmzius
            // 
            this.txtbAmzius.Location = new System.Drawing.Point(93, 77);
            this.txtbAmzius.Name = "txtbAmzius";
            this.txtbAmzius.Size = new System.Drawing.Size(100, 20);
            this.txtbAmzius.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Gatve";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Adresas:";
            // 
            // txtbNamas
            // 
            this.txtbNamas.Location = new System.Drawing.Point(93, 185);
            this.txtbNamas.Name = "txtbNamas";
            this.txtbNamas.Size = new System.Drawing.Size(100, 20);
            this.txtbNamas.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 341);
            this.Controls.Add(this.txtbNamas);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtbAmzius);
            this.Controls.Add(this.txtbGatve);
            this.Controls.Add(this.txtbMiestas);
            this.Controls.Add(this.txtbPavarde);
            this.Controls.Add(this.txtbVardas);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtbVardas;
        private System.Windows.Forms.TextBox txtbPavarde;
        private System.Windows.Forms.TextBox txtbMiestas;
        private System.Windows.Forms.TextBox txtbGatve;
        private System.Windows.Forms.TextBox txtbAmzius;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtbNamas;
    }
}

